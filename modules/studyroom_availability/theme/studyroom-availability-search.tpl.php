<?php

/**
 * @file
 * Default theme implementation to display the search.
 *
 * Available variables:
 *  - $search_form - The actual search form.
 */
?>
<?php print render($search_form); ?>
