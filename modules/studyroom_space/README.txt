-- NOTES --

If Views Bulk Operations (VBO) is installed prior to installing this
module, bulk deleting of spaces can be done.
